(defproject cljsinit "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  
  :dependencies [[org.clojure/clojure "1.7.0-alpha4"]
                 [org.clojure/clojurescript "0.0-2665"]
                 [datascript "0.7.2"]]
  
  :plugins [[lein-cljsbuild "1.0.4"]]
  
  :source-paths ["src"]
  
  :cljsbuild {:builds [{:id "test"
                        :source-paths ["src" "test"]
                        :notify-command ["phantomjs" "phantom/unit-test.js" "phantom/unit-test.html"]
                        :compiler {:optimizations :whitespace
                                   :pretty-print true
                                   :output-to "target/testable.js"}}]})
