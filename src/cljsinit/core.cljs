(ns cljsinit.core
  (:require
   [datascript :as d]))

(def conn (d/create-conn))

(def rules '[[(name ?t ?n)
              [?t :task/name ?n]]
             [(name ?t ?n)
              [(missing? $ ?t :task/name)]]])

(defn tasks
  [db name]
  (->>
    (d/q '[:find ?e ?id
           :in $ % ?name
           :where
           [?e :task/id ?id]
           (name ?e ?name)
           ]
      db
      rules
      name)))
