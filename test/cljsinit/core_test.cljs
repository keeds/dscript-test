(ns cljsinit.core-test
  (:require
   [cljs.test :refer-macros [deftest testing is]]
   [cljsinit.core :as c]
   [datascript :as d]))

(deftest base
  (testing "base"
    (let [db (-> (d/empty-db {})
               (d/db-with [{:db/id 1 :task/id 101 :task/name "A"}
                           {:db/id 2 :task/id 102}
                           {:db/id 3 :task/id 103}
                           {:db/id 4 :task/id 104 :task/name "D"}
                           ]))
          rules '[[(name ?t ?n)
                   [?t :task/name ?n]]
                  [(name ?t ?n)
                   [(missing? $ ?t :task/name)]]]]
      
      (is (= (d/q '[:find ?e ?id
                    :in $
                    :where
                    [?e :task/id ?id]
                    [(missing? $ ?e :task/name)]
                    ]
               db)
            #{[2 102] [3 103]}))

      (is (= (d/q '[:find ?e ?id
                    :in $ % ?name
                    :where
                    [?e :task/id ?id]
                    (name ?e ?name)
                    ]
               db
               rules
               "A")
            #{[1 101] [2 102] [3 103]}))
      
      (is (= (c/tasks db "A")
            #{[1 101] [2 102] [3 103]})))))
